#!/usr/bin/env node

const sha1 = require('sha1');
const salt = `eci n'est pas une pipe.`;

const itemCount = 5;

for (let i = 1, len = itemCount; i <= len; ++i) {
  const myNumber = i.toString();
  const hashString = sha1(`${salt}${myNumber}`);
  console.info(`${myNumber}. ${hashString}`);
}

function foo(cb) {
  cb('gerry');
}

foo((rv) => {
  console.info('rv:', rv);
});
