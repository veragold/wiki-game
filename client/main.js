/* eslint-disable */

(function() {

  var self = this;
  var data = __data__; // from game_data.js

  this.init = function() {
    this.startGameHandler();

    this.wikiImage = $(".wiki-image");
    this.choices = $(".choices");

  };

  this.displayResult = function(userAnswer, realAnswer) {
    var correct = userAnswer === realAnswer;
    var result = "";

    if (correct) {
      result = "correct! :)";
    } else {
      result = "WRONG";
    }

    // assign result to the html for display
    $(".problem-result").html(result);

  };

  this.submitAnswerButtonHandler = function() {
    var self = this;
    $("#answer-button").click(function() {
      var userAnswer = $('input[name=answer]:checked').val();
      self.displayResult(userAnswer, self.realAnswer);
    });
  };

  this.displayProblem = function() {

    this.realAnswer = data.answer;

    this.wikiImage.html('<img src="' + data.imageUrl + '" />');

    // format array of choices as radio buttons
    var newChoices = '';
    var i = 0;
    for (item of data.choices) {
      i++;
      newChoices += "<div><label><input name=answer type=radio value=" + i + ">" + item + "</label></div>";
    };

    this.choices.html(newChoices);

    this.submitAnswerButtonHandler();

  };

  this.startGameHandler = function() {
    $("#start-button").click(function() {
      $(".container").removeClass("hide");
      $("#start-button").hide();
      self.displayProblem();
    });
  };

  this.init();
})();
