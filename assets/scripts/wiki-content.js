#!/usr/bin/env node

const path = require('path');

const Promise = require('bluebird');
const Debussy = require('debussy');
const randomWiki = require('random-wiki');
const wikifetch = require('wikifetch-modern').default;

const sandboxDir = path.resolve(__dirname, '../../server/data/sandbox');

class GenerateQuestions {

  constructor() {
    this.topicList = [];
  }

  // @param void
  // @return {Promise}
  getTopicList() {
    const filename = path.resolve(sandboxDir, 'random-topics.txt');
    const FileReader = new Debussy(filename, {encoding: 'utf8'});
    FileReader.on('line', topic => {
      if (typeof topic === 'string' && topic.length > 0) {
        this.topicList.push(topic);
      }
    })

    return new Promise((resolve) => {
      FileReader.on('end', () => {
        resolve(this.topicList);
      })
    });
  }

  setTopicList() {
    console.info(`--> getting random wiki topic items...`);

    const MAX_NUM_QUERIES = 25;
    for (let i = 0; i < MAX_NUM_QUERIES; ++i) {
      randomWiki().then(response => console.info(response));
    }
  }

  wikiFetch(topic) {
    console.info(`--> getting title and image for [${topic}]`);

    wikifetch(encodeURIComponent(topic))
    .then(response => {
      const title = response.title;
      const sections = response.sections;
      const sectionNames = Object.keys(sections);

      // IMAGE
      for (const sectionName of sectionNames) {
        if ('images' in sections[sectionName]) {
          if (sectionName === title && sections[sectionName].images.length > 0) {
            console.info('\nTitle:', title);
            console.info('Image:', sections[sectionName].images[0]);
          }
        }
      }
    })
    .catch(err => {
      console.error('    ERROR:', `${err.statusCode} - ${err.message}\n    uri: [${err.options.uri}]`);
    }) ;
  }

  makeQuestions() {
    for (const topic of this.topicList) {
      this.wikiFetch(topic);
    }
  }

} // end GenerateQuestions class

const genQuery = new GenerateQuestions();

////genQuery.setTopicList()

genQuery.getTopicList()
  .then(topics => {
    genQuery.makeQuestions();
  });
