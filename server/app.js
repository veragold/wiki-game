'use strict';

const path = require('path');
const Koa = require('koa');
const Router = require('koa-66');
const serveStatic = require('koa-serve-static');

const PORT = 3000; ////nconf.get('KOA_PORT');
const app = new Koa();
const docRoot = path.resolve(__dirname, '../build');

const router = new Router();

const GameEngine = require('./game-engine');

app.use(router.routes());

router.get('/api/submitanswer/:id/:answer', ctx => {
  return GameEngine.submitAnswer(ctx.params.id, ctx.params.answer)
  .then(response => {
    ctx.body = response;
  })
  .catch(err => {
    ctx.body = err;
  })
});

router.get('/api/question/:questionIndex', ctx => {
  return GameEngine.getQuestion(ctx.params.questionIndex).then(response => {
    ctx.body = response;
  })
  .catch(err => {
    ctx.response.status = 418; // i'm a teapot :)'
    ctx.body = err;
  });
});

// Static
router.get('/', serveStatic(docRoot));
router.get('/bundle.js', serveStatic(docRoot));
router.get('/images/*(png|gif|jpe?g)', serveStatic(docRoot));

app.use(ctx => {
  ctx.response.set({'X-Powered-By': 'Catch All'});
  ctx.res.write('Not Found');
  ctx.res.end();
});


app.listen(PORT);

console.info(`Koa v2 listening on port: ${PORT.toString()}`); // eslint-disable-line no-console
