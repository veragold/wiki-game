const BASE_IMAGE_URL = 'https://upload.wikimedia.org/wikipedia/commons/thumb';
const DEFAULT_CHALLENGE = 'Which one of the following is associated with this image?';

https://upload.wikimedia.org/wikipedia/commons/thumb

exports.questions =
[
  {
    id: '5b597d15cd80d17c56db25303cb5b233eebd2849',
    imageUrl: `${BASE_IMAGE_URL}/0/02/Teruterubouzu.jpg/330px-Teruterubouzu.jpg`,
    challenge: DEFAULT_CHALLENGE,
    choices: [
      "Ghosts",
      "Potato Farming",
      "Teru teru bōzu",
      "Alien Anime"
    ],
    answer: "3"
  },

  {
    id: 'fdf60ff3b2f82ec6105a96f49cf0b886713d4eee',
    imageUrl: `${BASE_IMAGE_URL}//d/d2/Pythagorean.svg/260px-Pythagorean.svg.png`,
    challenge: DEFAULT_CHALLENGE,
    choices: [
      "How children learn their ABCs",
      "The Fibonacci series",
      "Design plan for baseball field",
      "Pythagorean theorem",
      "None of the above",
      "All of the above"
    ],
    answer: "4"
  },

  {
    id: '862d7d83547b20d549450dfd1c1fab6252c32502',
    imageUrl: `${BASE_IMAGE_URL}/e/e7/Georg_Cantor2.jpg/225px-Georg_Cantor2.jpg`,
    challenge: DEFAULT_CHALLENGE,
    choices: [
      "George Sands, Tomato Farmer Innovator",
      "George Cantor, Mathematician",
      "George Takei, Actor",
      "George Washington, American Revolutionary War Hero"
    ],
    answer: "2"
  }

]; // end exports.questions
