'use strict';

const Promise = require('bluebird');
const questions = require('./data/questions').questions;

class GameEngine {

  constructor() {
    this.idAnswerMap = {};
    this.createidAnswerMap();
    console.info('   ===> GameEngine started...');
    console.info('idAnswerMap:', this.idAnswerMap);
  }

  // hashed id => array index
  createidAnswerMap() {
    for (let i = 0; i < questions.length; ++i) {
      this.idAnswerMap[questions[i].id] = questions[i].answer;
    }
  }

  submitAnswer(id, answer) {
    return new Promise((resolve, reject) => {
      if (id in this.idAnswerMap) {
        const correctAnswer = this.idAnswerMap[id];
        const success = correctAnswer.toString() === answer.toString();
        const message = success ? 'correct answer' : 'incorrect answer';
        resolve({success, message});
      } else {
        reject({success: false, message: 'invalid id'});
      }
    });
  }

  getCorrectAnswer(id) {
    return new Promise((resolve, reject) => {
      if (id in this.idAnswerMap) {
        resolve({sucess: true, answer: this.idAnswerMap[id]});
      } else {
        reject({success: false, message: 'invalid id'});
      }
    });
  }

  getQuestion(idx) {
    return new Promise((resolve, reject) => {
      try {
        const question = questions[idx];
        const clientQ = {
          imageUrl: question.imageUrl,
          challenge: question.challenge,
          choices: question.choices,
          id: question.id
        }
        resolve(clientQ);
      } catch (ex) {
        if (typeof question !== 'object') {
          console.error('ERROR: could not retrieve question');
          reject({success: false, message: 'server cannot find question'});
          return;
        }
      }
    });
  }

}

module.exports = new GameEngine();
