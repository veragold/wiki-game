import angular from 'angular';

const BASE_URI = '/api/topsearches';

class TopAminoSearches {
  constructor($http) {
    this.$http = $http;
  }

  fetch(maxListing) {
    return this.$http.get(`${BASE_URI}/${maxListing}`);
  }

}

export default angular.module('services.top-amino-searches', [])
  .service('topAminoSearches', TopAminoSearches)
  .name;