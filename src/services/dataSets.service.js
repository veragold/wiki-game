import angular from 'angular';

class DataSets {

  static get HEADER_STATUS_MAP() {}

  get HEADER_STATUS_MAP() {
    return {
      // no error.
      0: 'NOERR',

      // format error—the server was unable to interpret the query.
      1: 'FORMERR',

      // name server problem or lack of information. Often also returned with the
      // same meaning as REFUSED.
      2: 'SERVFAIL',

      // Name does not exist: meaningful only from an authoritative name server.
      3: 'NXDOMAIN',

      // not implemented.
      4: 'NOTIMPL',

      // typically for policy reasons, for example, a zone transfer request.
      5: 'REFUSED'
    };
  }

}

export default angular.module('services.data-sets', [])
  .service('dataSets', DataSets)
  .name;