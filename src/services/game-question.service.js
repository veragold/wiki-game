import angular from 'angular';

class gameQuestionService {
  constructor($http) {
    this.$http = $http;
  }

  submitAnswer(id, userChoice) {
    return this.$http.get(`/api/submitanswer/${id}/${userChoice}`);
  }

  getQuestion(questionIndex) {
    return this.$http.get(`/api/question/${questionIndex}`);
  }

}
gameQuestionService.$inject = ['$http'];

export default angular.module('services.game-question', [])
  .service('gameQuestionService', gameQuestionService)
  .name;
