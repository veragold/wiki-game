import template from './question-component-template.html';

const indexToLetters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];

class GameQuestionController {

  static get $inject() {
    return ['$log'];
  }

  constructor($log) {
    this.$log = $log;
    this.$log.debug('inside Demo Controller');
  }

  indexToABCD(idx) {
    return indexToLetters[idx];
  }

}

export const gameQuestionComponent = {
  template,
  controller: GameQuestionController,
  bindings: {
    vm: '<'
  }
};
