const MAX_GUESSES = 2;
const TOTAL_QUESTIONS = 3;

export default class GameController {

  constructor($log, gameQuestionService) {
    this.$log = $log;
    this.gameQuestionService = gameQuestionService;

    this.isGameComplete = false;
    this.gameInProgress = false;
    this.activeQuestion = false;
    this.responseReturned = false;

    this.question = {};
    this.score = [];
    this.questionIndex = 0;

    this.userChoice = null;
    this.currentGameDisabled = false;
    this.guessCount = 0;
    this.isServerError = false;
    this.serverErrorMessage = '';
    this.isAnswerSubmitted = false;
    this.isNextQuestionPrompt = false;

  }

  startNewGame() {
    this.reset();
    this.getNewQuestion();
  }

  // @param {String} id
  // @param {Boolean} correct
  recordScore(id, correct) {
    this.score.push({id, correct});

    console.info('Score:', this.score);
  }

  newChoiceSelected() {
    this.isAnswerSubmitted = false;
  }

  calculateScore() {
    debugger;
    let numCorrect = 0;
    for (const score of this.score) {
      if (score.correct) {
        ++numCorrect;
      }
    }

    const numOfTotal = `${numCorrect}/${TOTAL_QUESTIONS}`;
    const percentage = '(' + Math.round(numCorrect / TOTAL_QUESTIONS * 100).toString() + '%)';

    this.displayScore = {numOfTotal, percentage};
  }

  submitUserChoice() {
    this.isServerError = false;
    this.isAnswerSubmitted = true;
    this.isNextQuestionPrompt = false;

    this.gameQuestionService.submitAnswer(this.question.id, this.userChoice)
    .then(response => {
      const data = response.data;
      this.responseReturned = true;
      this.verdict = data.success;

      ++this.guessCount

      if (data.success || this.guessCount >= MAX_GUESSES) {
        this.prohibitGuessing();
        this.recordScore(this.question.id, this.verdict);

        // If last question of game, display score instead of next question
        // prompt.
        if (this.questionIndex >= TOTAL_QUESTIONS) {
          this.calculateScore();
          this.isGameComplete = true;
          this.isNextQuestionPrompt = false;
        } else {
          this.isGameComplete = false;
          this.isNextQuestionPrompt = true;
        }
      }
    })
    .catch(err => {
      this.isServerError = true;
      this.responseReturned = false;
      console.error('ERROR:', err);
    });
  }

  prohibitGuessing() {
    this.isNextQuestionPrompt = true;
    this.currentGameDisabled = true;
  }

  getNewQuestion() {
    this.isNextQuestionPrompt = false;
    this.isServerError = false;

    this.currentGameDisabled = false;
    this.guessCount = 0;
    this.userChoice = null;
    this.responseReturned = false;

    this.gameQuestionService.getQuestion(this.questionIndex)
    .then(data => {
      this.question = data.data;
      this.isGameComplete = false;
      this.activeQuestion = true;
      this.gameInProgress = true;
      ++this.questionIndex;
    })
    .catch(err => {
      this.isServerError = true;
      this.$log.error('koa server error:', err);
      this.serverErrorMessage = `${err.status} ${err.statusText}`;
    })
  }

  reset() {
    this.isGameComplete = false;
    this.questionIndex = 0;
    this.gameInProgress = false;
    this.activeQuestion = false;
    this.isServerError = false;
    this.score = [];
  }

}

GameController.$inject = ['$log', 'gameQuestionService'];
