import './game.css';
import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './game.routes';
import GameController from './game.controller';

import gameQuestionService from '../../services/game-question.service';

import {gameQuestionComponent} from './components/question-component';

export default angular.module('app.game', [uirouter, gameQuestionService])
  .config(routing)
  .controller('GameController', GameController)
  .component('gameQuestion', gameQuestionComponent)
  .name;
