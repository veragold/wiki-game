import 'bootstrap/dist/css/bootstrap.css';

import angular from 'angular';
import uirouter from 'angular-ui-router';
import routing from '../app.config';

// The Wiki Game
import game from '../features/game';

angular.module('app', [uirouter, game])
  .config(routing);
